#!/usr/bin/python3

from tkinter import *
import tkinter.ttk as ttk
# import tkinter.messagebox
import youtube_dl
import threading


class MainApplication:
    #class variables
    top = None
    lblURL = None
    entURL = None
    rdoStandard = None
    rdoCustom = None
    varDownloadMethod = None
    btnResolutions = None
    lblResolutions = None
    cboResolutions = None
    AvailableResolutions = None
    FileMetadata = []
    chkboxSubtitles = None
    checkSubtitles = None
    lblFileInfo = None
    treeFileInfo = None
    scrollInfo = None
    scrollXInfo = None
    lblEvents = None
    treeEvents = None
    scrollEvents = None
    scrollXEvents = None
    btnFrame = None
    btnDownload = None
    btnCancel = None
    lblStatus = None
    pgsDownload = None
    DLVthread = None
    
    
    def __init__(self):
        self.top = Tk()
        self.top.title('Youtube-DL Frontend')
        self.lblURL = ttk.Label(self.top, text='Enter the URL of the video/playlist you want to download')
        self.entURL = Entry(self.top, width=50)
        self.varDownloadMethod=IntVar()
        self.rdoStandard = Radiobutton(self.top, text='Quick Download', variable=self.varDownloadMethod, value=0, command=self.resolution_change)
        self.rdoCustom = Radiobutton(self.top, text='Custom Resolution', variable=self.varDownloadMethod, value=1, command=self.resolution_change)
        self.btnResolutions = ttk.Button(self.top, text='Check for available resolutions', command=self.threaded_resolution_get)
        self.lblResolutions = ttk.Label(self.top, text='Available Resolutions')
        self.cboResolutions = ttk.Combobox(self.top, values=self.AvailableResolutions, state='readonly')
        self.checkSubtitles = IntVar()
        self.checkSubtitles.set(0)
        self.chkboxSubtitles = Checkbutton(self.top, text='Include Subtitles?', variable=self.checkSubtitles, onvalue=1, offvalue=0)
        self.lblFileInfo = ttk.Label(self.top, text='Custom Video Information')
        self.treeFileInfo = ttk.Treeview(self.top, columns='one', height=4, selectmode='none')
        self.treeFileInfo['columns']=('type', 'info')
        self.treeFileInfo.column('#0', anchor='center', width=0, stretch=NO)
        self.treeFileInfo.column('type', anchor='center')
        self.treeFileInfo.column('info')
        self.treeFileInfo.heading('#0', text='')
        self.treeFileInfo.heading('type', text='Type')
        self.treeFileInfo.heading('info', text='Info')
        self.scrollInfo = ttk.Scrollbar(self.top, orient='vertical', command=self.treeFileInfo.yview)
        self.scrollXInfo = ttk.Scrollbar(self.top, orient='horizontal', command=self.treeFileInfo.xview)
        self.treeFileInfo.configure(yscroll=self.scrollInfo.set)
        self.treeFileInfo.configure(xscroll=self.scrollXInfo.set)
        self.lblEvents = ttk.Label(self.top, text='Events')
        self.treeEvents = ttk.Treeview(self.top, columns='one', height=6, selectmode='none')
        self.treeEvents['columns']=('type', 'event')
        self.treeEvents.column('#0', anchor='center', width=0, stretch=NO)
        self.treeEvents.column('type', anchor='center')
        self.treeEvents.column('event')
        self.treeEvents.heading('#0', text='')
        self.treeEvents.heading('type', text='Event Type')
        self.treeEvents.heading('event', text='What happened?')
        self.treeEvents.tag_configure('event', background='lightgreen')
        self.treeEvents.tag_configure('warning', background='orange')
        self.treeEvents.tag_configure('error', background='pink')
        self.scrollEvents = ttk.Scrollbar(self.top, orient='vertical', command=self.treeEvents.yview)
        self.scrollXEvents = ttk.Scrollbar(self.top, orient='horizontal', command=self.treeEvents.xview)
        self.treeEvents.configure(yscroll=self.scrollEvents.set)
        self.treeEvents.configure(xscroll=self.scrollXEvents.set)
        self.btnFrame = Frame(self.top)
        self.btnDownload = ttk.Button(self.btnFrame, text='Download', state=DISABLED, command=lambda: self.threaded_download(None))
        self.top.bind('<Return>', self.threaded_download)
        self.btnCancel = ttk.Button(self.btnFrame, text='Cancel', state=DISABLED, command=None)
        self.lblStatus = ttk.Label(self.top, text='Status: IDLE', wraplength=450)
        self.pgsDownload = ttk.Progressbar(self.top, orient=HORIZONTAL, length=100, mode='determinate')
        self.resolution_change()
        
        
    def build_ui(self):
        self.top.columnconfigure(0, weight=1)
        self.top.columnconfigure(1, weight=1)
        self.lblURL.grid(row=0, columnspan=3, sticky='W')
        self.entURL.grid(row=1, columnspan=3, sticky='EW')
        self.entURL.focus()
        self.rdoStandard.grid(row=2, column=0, sticky='EW', padx=8, pady=8)
        self.rdoCustom.grid(row=2, column=1, sticky='EW', padx=8, pady=8)
        self.chkboxSubtitles.grid(row=3, columnspan=3, sticky='EW')
        self.btnResolutions.grid(row=4, columnspan=3, sticky='EW', padx=8, pady=8)
        self.lblResolutions.grid(row=5, columnspan=3, sticky='W')
        self.cboResolutions.grid(row=6, columnspan=3, sticky='EW')
        self.cboResolutions.bind("<<ComboboxSelected>>", self.fill_file_info)
        ttk.Separator(self.top, orient=HORIZONTAL).grid(row=7, columnspan=3, sticky='EW')
        self.lblFileInfo.grid(row=8, columnspan=3, sticky='W')
        self.treeFileInfo.grid(row=9, columnspan=2, sticky='NSEW')
        self.scrollInfo.grid(row=9, column=2, sticky='NS')
        self.scrollXInfo.grid(row=10, columnspan=2, sticky='EW')
        self.lblEvents.grid(row=11, columnspan=3, sticky='W')
        self.treeEvents.grid(row=12, columnspan=2, sticky='NSEW')
        self.scrollEvents.grid(row=12, column=2, sticky='NS')
        self.scrollXEvents.grid(row=13, columnspan=2, sticky='EW')
        self.btnFrame.grid(row=14, columnspan=3, sticky='EW')
        self.btnFrame.columnconfigure(0, weight=1)
        self.btnFrame.columnconfigure(1, weight=1)
        self.btnDownload.grid(row=0, column=0, sticky='EW', padx=8, pady=8)
        self.btnCancel.grid(row=0, column=1, sticky='EW', padx=8, pady=8)
        self.pgsDownload.grid(row=15, columnspan=3, sticky='EW')
        self.lblStatus.grid(row=16, columnspan=2, sticky='W')
        center_window(self.top)
        
        
    def threaded_download(self, event):
        DLSthread = threading.Thread(target=self.download_start_ui_change)
        DLSthread.start()
        self.DLVthread = threading.Thread(target=self.download_video)
        self.DLVthread.start()
        
        
    def download_video(self):
        audio_setting = None
        subtitles = None
        if self.checkSubtitles.get() == 1:
            subtitles = True
        else:
            subtitles = False
        if self.varDownloadMethod.get() == 0:
            ydl_opts = {'format': 'mp4', 'writesubtitles': subtitles, 'ignoreerrors': 'true', 'progress_hooks': [self.my_hook], 'logger': MyLogger()}
            download_link = self.entURL.get()
            with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                ydl.download([download_link])
        if self.varDownloadMethod.get() == 1:
            resolution = self.cboResolutions.get()
            dataurl = None
            m3u8 = None
            native = None
            for i in self.FileMetadata:
                if i['format'] == resolution:
                    for l in i:
                        datatype = l
                        datadata = i[l]
                        if datatype == 'acodec':
                            if datadata == 'none':
                                audio_setting = '+bestaudio'
                            else:
                                audio_setting = ''
                        if datatype == 'url':
                            dataurl = datadata
                        if datatype == 'protocol':
                            if datadata[:4] == 'm3u8':
                                m3u8 = True
                            else:
                                m3u8 = False
                            if datadata == 'm3u8_native5':
                                native = True
                            else:
                                native = False
            if resolution != 'resolution':
                if resolution[:1].isdigit():
                    resolution = int(resolution[:resolution.find('-')].strip())
                    ydl_opts = {'format': f'{resolution}'+audio_setting, 'writesubtitles': subtitles, 'ignoreerrors': 'true', 'progress_hooks': [self.my_hook], 'logger': MyLogger()}
                    download_link = self.entURL.get()
                    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                        ydl.download([download_link])
                else:
                    ydl_opts = {'format': 'mp4'+audio_setting, 'writesubtitles': subtitles, 'ignoreerrors': 'true', 'progress_hooks': [self.my_hook], 'logger': MyLogger()}
                    if m3u8:
                        end = dataurl.find('m3u8')
                        dataurl = dataurl[:end+4]
                    if native:
                        end = dataurl.find('.mp4')
                        dataurl = dataurl[:end+4]
                    download_link = dataurl
                    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                        ydl.download([download_link])
        self.download_end_ui_change()
        self.resolution_change()
        
        
    def download_start_ui_change(self):
        self.lblStatus.config(text='Status: Getting file information')
        self.entURL.config(background='#f0f0f0', state=DISABLED)
        self.rdoStandard.config(state=DISABLED)
        self.rdoCustom.config(state=DISABLED)
        self.cboResolutions.config(state=DISABLED)
        self.btnResolutions.config(state=DISABLED)
        self.chkboxSubtitles.config(state=DISABLED)
        self.btnDownload.config(state=DISABLED)
        self.top.unbind('<Return>')
        self.btnCancel.config(state=NORMAL)
        
        
    def download_end_ui_change(self):
        self.lblStatus.config(text='Status: IDLE')
        self.entURL.config(background='white', state=NORMAL)
        self.rdoStandard.config(state=NORMAL)
        self.rdoCustom.config(state=NORMAL)
        self.entURL.delete(0, 'end')
        self.entURL.focus()
        self.cboResolutions.config(state='readonly')
        self.cboResolutions['values']=('')
        self.cboResolutions.set('')
        self.checkSubtitles.set(0)
        self.pgsDownload['value'] = 0
        self.btnResolutions.config(state=NORMAL)
        self.chkboxSubtitles.config(state=NORMAL)
        self.btnDownload.config(state=NORMAL)
        self.top.bind('<Return>', self.threaded_download)
        self.btnCancel.config(state=DISABLED)
        self.top.title('Youtube-DL Frontend')
        for i in self.treeFileInfo.get_children():
            self.treeFileInfo.delete(i)
        
        
    def threaded_resolution_get(self):
        UIChange = threading.Thread(target=self.fetching_resolution_ui_change)
        UIChange.start()
        ResolutionGet = threading.Thread(target=self.get_resolutions)
        ResolutionGet.start()
        
        
    def fetching_resolution_ui_change(self):
        self.lblStatus.config(text='Fetching available resolutions...')
        self.btnResolutions.config(state=DISABLED)
        
        
    def get_resolutions(self):
        resolutions = []
        ydl_opts = {}
        url = self.entURL.get()
        valid = self.valid_link(url) #check to see if the link is valid
        if valid:
            try:
                self.entURL.config(background='white')
                with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                    meta = ydl.extract_info(url, download=False)
                    formats = meta.get('formats', meta)
                for f in formats:
                    self.FileMetadata.append(f)
                    if f['ext'] == 'webm':
                    	None
                    else:
                    	resolutions.append(f['format']) #filter to only formats
                self.cboResolutions['values'] = resolutions # sets combobox values to available resolutions
                self.btnDownload.config(state=NORMAL)
                self.lblStatus.config(text='Status: IDLE')
                self.cboResolutions.current(0)
            except:
                self.log_entry('Error', 'Error getting file information', "error")
        else:
            self.entURL.config(background='red')
            self.entURL.delete(0, 'end')
            self.entURL.focus()
            self.lblStatus.config(text='Status: INVALID URL')
        self.btnResolutions.config(state=NORMAL)
                
        
    def fill_file_info(self, event):
        for i in self.treeFileInfo.get_children():
            self.treeFileInfo.delete(i)
        selected_file = self.cboResolutions.get()
        for i in self.FileMetadata:
            if i['format'] == selected_file:
                for l in i:
                    # print(l)
                    if l == 'format':
                        self.treeFileInfo.insert('', 'end', text='', values=(l, str(i[l])))
                    elif l == 'ext':
                        self.treeFileInfo.insert('', 'end', text='', values=(l, str(i[l])))
                    elif l == 'vcodec':
                        self.treeFileInfo.insert('', 'end', text='', values=(l, str(i[l])))
                    elif l == 'acodec':
                        self.treeFileInfo.insert('', 'end', text='', values=(l, str(i[l])))
                    else:
                        None
        
        
    def valid_link(self, link): # function to check validity of the link   
        extractors = youtube_dl.extractor.gen_extractors()
        for e in extractors:
            if e.suitable(link) and e.IE_NAME != 'generic':
                return True
        return False
    
    
    def my_hook(self, hook):
        if hook['status'] == 'downloading':
            try:
                self.lblStatus.config(text='DOWNLOADING VIDEO: ' + hook['filename'])
                # self.log_entry('Event', 'Started downloading ' + hook['filename'], "event")
            except:
                self.lblStatus.config(text='DOWNLOADING VIDEO: INVALID FILENAME')
            self.pgsDownload['value'] = float(hook['_percent_str'].strip('%'))
            self.top.title('Downloading: ' + hook['_percent_str'])
        if hook['status'] == 'finished':
            download_done = 'Finished downloading ' + hook['filename']
            self.log_entry('Completed', download_done, "event")
            
            
    def resolution_change(self):
        if self.varDownloadMethod.get() == 1:
            self.cboResolutions.config(state='readonly')
            self.cboResolutions['values']=('')
            self.cboResolutions.set('')
            self.btnResolutions.config(state=NORMAL)
            self.btnDownload.config(state=DISABLED)
            self.top.unbind('<Return>')
        else:
            self.cboResolutions['values']=('')
            self.cboResolutions.set('')
            self.cboResolutions.config(state=DISABLED)
            self.btnResolutions.config(state=DISABLED)
            for i in self.treeFileInfo.get_children():
                self.treeFileInfo.delete(i)
            self.btnDownload.config(state=NORMAL)
            self.top.bind('<Return>', self.threaded_download)
            
            
    def cancel_download(self):
        #self.DLVthread.exit()
        #self.DLVthread.stop()
        if self.DLVthread.is_alive():
            # print('Download thread is still alive. Kill it!')
            self.DLVthread.join(0)
            self.DLVthread.exit()
            self.download_end_ui_change()
        else:
            pass
            
            
    def log_entry(self, level, log, tag):
        self.treeEvents.insert('', 'end', text='', values=(level,log), tags=tag)
        
        
class MyLogger(object):
    global root
    def debug(self, msg):
        pass
    
    
    def warning(self, msg):
        root.log_entry('Warning', msg, "warning")
    
    
    def error(self, msg):
        root.log_entry('Error', msg, "error")
        
        
def center_window(window):
    window.update_idletasks()
    ws = window.winfo_screenwidth()
    hs = window.winfo_screenheight()
    ww = window.winfo_width()
    wh = window.winfo_height()
    screen_half_x = (ws/2) - (ww/2)
    screen_half_y = ((hs/2) - (wh/2)) * 0.5
    window.geometry('%dx%d+%d+%d' % (ww, wh, screen_half_x, screen_half_y))
        
        
if __name__ == '__main__':
    root = MainApplication()
    root.build_ui()
    root.top.mainloop()
